﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    class HackerRankInAString
    {
        public HackerRankInAString()
        {
            Console.WriteLine("--- HackerRank in a string ---");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            string hr = "hackerrank";

            int lastPos = 0;
            string hr1 = "";
            for (int i = 0; i < hr.Length; i++)
            {
                for (int j = lastPos; j < kalimat.Length; j++)
                {
                    if (hr.Substring(i, 1) == kalimat.Substring(j,1))
                    {
                        hr1 += kalimat.Substring(j, 1);
                        lastPos = j + 1;
                        break;
                    }
                }
            }

            if(hr == hr1)
                Console.WriteLine("YES");
            else
                Console.WriteLine("NO");

            Console.ReadKey();
        }
    }
}
