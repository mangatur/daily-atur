﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic08
{
    class Parkir
    {
        public Parkir()
        {
            Console.WriteLine("-- Parkir ---");
            Console.Write("Masuk parkir:");
            int[] masuk = Array.ConvertAll(Console.ReadLine().Split(','), wkt => int.Parse(wkt));
            Console.WriteLine();
            Console.Write("Keluar parkir:");
            int[] keluar = Array.ConvertAll(Console.ReadLine().Split(','), wkt => int.Parse(wkt));
            TimeSpan ts = new DateTime(keluar[0], keluar[1], keluar[2], keluar[3], keluar[4], keluar[5]) - new DateTime(masuk[0], masuk[1], masuk[2], masuk[3], masuk[4], masuk[5]);

            //int[] lama = Array.ConvertAll(Console.ReadLine().Split(':'), wkt => int.Parse(wkt));

            int[] lama = new int[3];
            int detik = (int)ts.TotalSeconds;

            lama[0] = detik / 3600;
            detik -= lama[0] * 3600;

            lama[1] = detik / 60;
            detik -= lama[1] * 60;

            lama[2] = detik;

            detik = (int)ts.TotalSeconds;

            double bayar = 0;

            if (detik <= 28800)
                bayar = (lama[0] + (lama[2] > 0 ? 1 : 0)) * 1000;
            else if (detik <= 86400)
                bayar = 8000;
            else
                bayar = 15000 + ((lama[0] - 24) * 1000) + (lama[2] > 0 ? 1000 : 0);

            Console.WriteLine("Detik : {0}", detik);

            foreach (var item in lama)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Bayar : {0}", bayar);
            Console.ReadKey();
        }
    }
}
