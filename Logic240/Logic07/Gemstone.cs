﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    class Gemstone
    {
        public Gemstone()
        {
            Console.WriteLine("--- Gemstone ---");
            Console.Write("Masukkan string arr : ");
            string[] kata = Console.ReadLine().Split(' ');

            string lib = "";
            string lib2 = "";

            //ambil koleksi library
            for (int i = 0; i < kata[0].Length; i++)
            {
                if (!lib.Contains(kata[0].Substring(i, 1)))
                    lib += kata[0].Substring(i, 1);
            }

            //bandingkan dgn kata selanjutnya
            for (int i = 1; i < kata.Length; i++)
            {
                for (int j = 0; j < lib.Length; j++)
                {
                    if (kata[i].Contains(lib.Substring(j, 1)))
                        lib2 += lib.Substring(j, 1);
                }
                lib = lib2;
                lib2 = "";
            }

            Console.WriteLine(lib + " => " + lib.Length);
            Console.ReadKey();
        }
    }

    class MakingAnagrams
    {
        public MakingAnagrams()
        {
            Console.WriteLine("--- Making Anagrams ---");
            Console.Write("Masukkan string arr : ");
            string[] kata = Console.ReadLine().Split(' ');

            string lib = kata[0];

            for (int i = 0; i < kata[1].Length; i++)
            {
                if (lib.IndexOf(kata[1].Substring(i, 1)) > -1)
                    lib = lib.Remove(lib.IndexOf(kata[1].Substring(i, 1)), 1);
                else
                    lib += kata[1].Substring(i, 1);
            }

            Console.WriteLine(lib + " ==> " + lib.Length);
            Console.ReadKey();
        }
    }
}
