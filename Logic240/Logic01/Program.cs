﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic01
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("--- Kumpulan Logic 01 ---");
            Console.ReadKey();
            //Soal01 soal01 = new Soal01();
            //Soal02 soal02 = new Soal02();
            //Soal04 soal04 = new Soal04(9);
        }

        public Program()
        {
            string lanjut = "Y";
            while (lanjut.ToUpper() == "Y")
            {
                Console.WriteLine("--- Kumpulan Logic 01 ---");
                Console.WriteLine("Masukkan nomor soal:");
                int soal = int.Parse(Console.ReadLine());
                switch (soal)
                {
                    case 1:
                        Soal01 soal01 = new Soal01();
                        break;
                    case 4:
                        Console.Write("Masukkan nilai N :");
                        int n = int.Parse(Console.ReadLine());
                        Soal04 soal04 = new Soal04(n);
                        break;
                    default:
                        break;
                }
                Console.Write("Lanjut Logic 1 ketik Y/y : ");
                lanjut = Console.ReadLine();
            }
        }
    }
}
