﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic08
{
    class SudutJam
    {
        public SudutJam()
        {
            Console.WriteLine("-- Sudut Jam --");
            Console.Write("Masukkan waktu : ");
            decimal[] waktu = Array.ConvertAll(Console.ReadLine().Split(':'), wkt => decimal.Parse(wkt));

            //30 => sudut setiap jam
            //2 => 60 menit/30 derajat
            //6 => sudut setiap menit
            decimal sudut = Math.Abs((waktu[0] * 30) + (waktu[1] / 2) - (waktu[1] * 6));
            sudut = sudut < 180 ? sudut : 360 - sudut;
            Console.WriteLine("Sudut : {0}", sudut);
            Console.ReadKey();
        }
    }
}
