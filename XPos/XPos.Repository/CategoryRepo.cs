﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPos.DataModel;

namespace XPos.Repository
{
    public class CategoryRepo
    {
        //List
        public static List<Category> All()
        {
            List<Category> result = new List<Category>();
            using (var db = new XPosContext())
            {
                result = db.Categories.ToList();
            }
            return result;
        }

        //Create
        public static ResponseResult Update(Category entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    if (entity.Id == 0)
                    {
                        // Create
                        entity.CreateBy = "Atur";
                        entity.CreateDate = DateTime.Now;
                        db.Categories.Add(entity);
                        db.SaveChanges();
                        result.Message = "Created";
                        result.Entity = entity;
                    }
                    else
                    {
                        //Edit
                        Category category = db.Categories
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                        if (category == null)
                        {
                            result.Success = false;
                        }
                        else
                        {
                            category.Initial = entity.Initial;
                            category.Name = entity.Name;
                            category.Active = entity.Active;
                            category.ModifyBy = "Atur";
                            category.ModifyDate = DateTime.Now;
                            db.SaveChanges();

                            result.Message = "Updated";
                            result.Entity = entity;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static Category ById(long id)
        {
            Category result = new Category();
            try
            {
                using (var db = new XPosContext())
                {
                    result = db.Categories
                        .Where(o => o.Id == id)
                        .FirstOrDefault();
                    //if (result == null)
                    //{
                    //    result = new Category();
                    //}
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new Category() : result;
        }

        public static ResponseResult Delete(long id)
        {
            ResponseResult result = new ResponseResult();
            try
            {

                using (var db = new XPosContext())
                {
                    Category category = db.Categories
                        .Where(o => o.Id == id)
                        .FirstOrDefault();

                    Category oldCategory = category;
                    result.Entity = oldCategory;

                    if (category != null)
                    {
                        db.Categories.Remove(category);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Category not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }

    public class CategoryRepoSP
    {
        public static List<Category> All()
        {
            List<Category> result = new List<Category>();
            try
            {
                SqlCommand sc = new SqlCommand();
                string sqlConn = ConfigurationManager.ConnectionStrings["XPosContext"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(sqlConn))
                {
                    conn.Open();
                    sc.Connection = conn;
                    sc.CommandType = CommandType.StoredProcedure;
                    sc.CommandText = "sp_GetCategory";
                    sc.Parameters.AddWithValue("@pId", 0);
                    SqlDataReader dr = sc.ExecuteReader();
                    while (dr.Read())
                    {
                        Category entity = new Category();
                        entity.Id = int.Parse(dr["Id"].ToString());
                        entity.Initial = dr["Initial"].ToString();
                        entity.Name = dr["Name"].ToString();
                        entity.Active = (bool)dr["Active"];

                        entity.CreateBy = dr["Initial"].ToString();
                        entity.CreateDate = (DateTime)dr["CreateDate"];
                        entity.ModifyBy = dr["ModifyBy"].ToString();
                        entity.ModifyDate = dr.IsDBNull(dr.GetOrdinal("ModifyDate")) ? null : (DateTime?)dr["ModifyDate"];

                        result.Add(entity);
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public static Category ById(long id)
        {
            Category result = new Category();
            try
            {
                SqlCommand sc = new SqlCommand();
                string sqlConn = ConfigurationManager.ConnectionStrings["XPosContext"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(sqlConn))
                {
                    conn.Open();
                    sc.Connection = conn;
                    sc.CommandType = CommandType.StoredProcedure;
                    sc.CommandText = "sp_GetCategory";
                    sc.Parameters.AddWithValue("@pId", id);
                    SqlDataReader dr = sc.ExecuteReader();
                    while (dr.Read())
                    {
                        Category entity = new Category();
                        entity.Id = int.Parse(dr["Id"].ToString());
                        entity.Initial = dr["Initial"].ToString();
                        entity.Name = dr["Name"].ToString();
                        entity.Active = (bool)dr["Active"];

                        entity.CreateBy = dr["Initial"].ToString();
                        entity.CreateDate = (DateTime)dr["CreateDate"];
                        entity.ModifyBy = dr["ModifyBy"].ToString();
                        entity.ModifyDate = dr.IsDBNull(dr.GetOrdinal("ModifyDate")) ? null : (DateTime?)dr["ModifyDate"];

                        result = entity;
                        break;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public static ResponseResult Update(Category entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                SqlCommand sc = new SqlCommand();
                string sqlConn = ConfigurationManager.ConnectionStrings["XPosContext"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(sqlConn))
                {
                    conn.Open();
                    sc.Connection = conn;
                    sc.CommandType = CommandType.StoredProcedure;
                    sc.CommandText = "sp_UpdateCategory";
                    sc.Parameters.AddWithValue("@pId", entity.Id);
                    sc.Parameters.AddWithValue("@pInitial", entity.Initial);
                    sc.Parameters.AddWithValue("@pName", entity.Name);
                    sc.Parameters.AddWithValue("@pActive", entity.Active);
                    sc.Parameters.AddWithValue("@pUserName", "Atur");
                    sc.ExecuteNonQuery();

                    result.Message = entity.Id == 0 ? "Created" : "Updated";
                    result.Entity = entity;

                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }
    }
}
